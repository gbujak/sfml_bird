#include <vector>
#include <string>
#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Window/Keyboard.hpp>

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600
#define FPS 60

#define PLAYER_RADIUS 10
#define PLAYER_X 100
#define GRAVITY 800 // PER SECOND
#define WALL_GAP 200
#define WALL_WIDTH 50

#define WALL_SPAWN_DELAY 1
#define WALL_SPAWN_TIME_GAP 10
#define WALL_ACCELERTION -0.005

#define JUMP_DELTA (float) -400

#define FONTSIZE 50

bool key_pressed;

class Wall {
    static float WALL_DELTA;
    float m_gap_pos;
    float m_x_pos;
    bool m_can_collide;
    sf::RectangleShape m_wall;
    sf::RectangleShape m_hole;
    public:
    Wall(): m_gap_pos (rand() % (WINDOW_HEIGHT - WALL_GAP)),
            m_x_pos   (WINDOW_WIDTH), m_can_collide(true) {
        m_wall.setSize({ WALL_WIDTH, WINDOW_HEIGHT });
        m_wall.setFillColor(sf::Color(155, 255, 155));
        m_hole.setSize({ WALL_WIDTH, WALL_GAP });
        m_hole.setFillColor(sf::Color(20, 20, 100));
    }
    static void accelerate(float x) { WALL_DELTA += x; }
    float gap_pos()  const { return m_gap_pos; }
    float x_pos()    const { return m_x_pos;   }
    bool offscreen() const { return m_x_pos + WALL_WIDTH < 0; }
    bool can_collide() const { return m_can_collide; }
    void passed(unsigned char& score) { 
        m_can_collide = false;
        ++score;
    }
    void  move() {
        m_x_pos += WALL_DELTA / FPS;
        m_wall.setPosition({ m_x_pos, 0 });
        m_hole.setPosition({ m_x_pos, m_gap_pos });
        WALL_DELTA += WALL_ACCELERTION / FPS;
    }
    void draw(sf::RenderTarget& win,
              sf::RenderStates states = sf::RenderStates::Default) const {
        win.draw(m_wall);
        win.draw(m_hole);
    }
};
float Wall::WALL_DELTA = -200;

class Player : sf::Drawable {
    float m_position_y;
    float m_delta_y;
    sf::CircleShape m_shape;
    public:
    Player(float pos, float delta_y)
        : m_position_y(pos), m_delta_y(delta_y) {
        m_shape.setFillColor(sf::Color(255, 155, 155));
        m_shape.setRadius(PLAYER_RADIUS + 10);
        m_shape.setOrigin(PLAYER_RADIUS + 10, PLAYER_RADIUS + 10);
    }
    void move() {
        m_delta_y += GRAVITY / FPS;
        m_position_y += m_delta_y / FPS;
        m_shape.setPosition(PLAYER_X, m_position_y);
    }
    void draw(sf::RenderTarget& win,
              sf::RenderStates states = sf::RenderStates::Default) const {
        win.draw(m_shape, states);
    }
    void set_delta_y(float delta_y) {
        m_delta_y = delta_y;
    }
    bool collides(std::vector<Wall>& walls, unsigned char& score) {
        if (m_position_y > WINDOW_HEIGHT) return true;
        for (Wall& wall : walls) {
            if (wall.can_collide() == false) continue;
            float wall_x_pos = wall.x_pos();
            if (PLAYER_X > wall_x_pos - PLAYER_RADIUS
             && PLAYER_X < wall_x_pos + WALL_WIDTH + PLAYER_RADIUS) {
                float wall_gap_pos = wall.gap_pos();
                if (m_position_y < wall_gap_pos + PLAYER_RADIUS
                 || m_position_y > wall_gap_pos + WALL_GAP - PLAYER_RADIUS)
                    return true;
            }
            if (PLAYER_X > (wall.x_pos() + WALL_WIDTH - PLAYER_RADIUS))
                wall.passed(score);
        }
        return false;
    }
};

bool space_pressed() {
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
        if (!key_pressed) {
            key_pressed = true;
            return true;
        } else return false;
    } else {
        key_pressed = false;
        return false;
    }
}

void draw_score(sf::RenderWindow* window, sf::Text* text, unsigned char score) {
    std::string score_str = std::to_string( (unsigned int) score );
    float xpos = WINDOW_WIDTH / 2 - FONTSIZE * score_str.length() / 2;
    text->setString(score_str);
    text->setPosition({xpos, FONTSIZE});
    window->draw(*text);
}

void game(sf::RenderWindow* window, sf::Event* event, sf::Text* text) {
    // INIT
    long unsigned ticks = 0;
    unsigned char score = 0;
    Player player = { (float) WINDOW_HEIGHT/2, 0.0f };
    std::vector<Wall> walls;

    while (true) {
        window->clear(sf::Color(20, 20, 100));

        // SPAWN
        if (ticks > WALL_SPAWN_DELAY * FPS && ticks % ((long unsigned) (2 * (float) FPS)) == 0)
            walls.push_back({});

        // WINDOW EVENTS
        while (window->pollEvent(*event))
            if (event->type == sf::Event::Closed) return;

        // INPUTS
        if (space_pressed()) player.set_delta_y(JUMP_DELTA);

        // UPDATE
        for (auto& w : walls) w.move();
        player.move();
        if (player.collides(walls, score)) return;
        if (walls.size() > 0 && walls.begin()->offscreen())
            walls.erase(walls.begin());

        // DRAW
        for (auto& w : walls) w.draw(*window);
        player.draw(*window);
        draw_score(window, text, score);

        // TICK
        window->display();
        ++ ticks;
    }
}

int main(void) {
    srand(time(nullptr));
    key_pressed = false;

    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;

    sf::RenderWindow window(
        sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT),
        "SFML_bird", sf::Style::Default, settings
    );
    sf::Event event;
    window.setPosition(sf::Vector2i(
        sf::VideoMode::getDesktopMode().width / 2 - WINDOW_WIDTH / 2,
        sf::VideoMode::getDesktopMode().height / 2 - WINDOW_HEIGHT / 2
    ));
    window.clear();
    window.display();
    window.setFramerateLimit(FPS);

    sf::Font font;
    if (!font.loadFromFile("font/Inconsolata.ttf")) {
        std::cerr << "cannot load font, aborting" << std::endl;
        abort();
    }
    sf::Text text;
    text.setFont(font);
    text.setCharacterSize(FONTSIZE);
    text.setFillColor(sf::Color::White);

    game(&window, &event, &text);

    window.close();
}